import React from "react";
import './App.css';
import CreateNumbers from "./CreateNumbers";

class App extends React.Component {
    state = {
        numbers: [1, 2, 3, 4, 5]
    }

    changeNum = () => {
        const max = 36;
        const min = 5;

        const numbers = this.state.numbers;
        let i;
        for (i = 0; i < numbers.length; i++) {
            numbers[i] = Math.floor(Math.random() * (max-min)) + min;
            for (let j = i - 1; j >= 0; j --) {
                if (numbers[i] === numbers[j]) {
                    numbers[i] = Math.floor(Math.random() * (max-min)) + min;
                }
            }
        }
        let a;
        for (i = 0; i < numbers.length; i++) {
            for (let j = 0; j < numbers.length - 1; j++) {
                if (numbers[j] > numbers[j + 1]) {
                    a = numbers[j];
                    numbers[j] = numbers[j + 1];
                    numbers[j + 1] = a;
                }
            }
        }
        this.setState({numbers});
    };

    render() {
        return (
            <div className="App">
                <div>
                    <button className="new-numbers-btn" onClick={this.changeNum}>New numbers</button>
                </div>
                <CreateNumbers number={this.state.numbers[0]}/>
                <CreateNumbers number={this.state.numbers[1]}/>
                <CreateNumbers number={this.state.numbers[2]}/>
                <CreateNumbers number={this.state.numbers[3]}/>
                <CreateNumbers number={this.state.numbers[4]}/>
            </div>
        );
    }

}

export default App;
